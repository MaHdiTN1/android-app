package org.rhyv.testapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Display;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import org.rhyv.testapp.ui.items.ItemFragment;

// Has no real use at this moment.
public class DetailActivity extends AppCompatActivity {

  @Override
  protected void onCreate( Bundle savedInstanceState ){
    super.onCreate( savedInstanceState );
    setContentView( R.layout.activity_detail );

    Intent in   = getIntent();
    int index   = in.getIntExtra( ItemFragment.EXTRA_ITEM_INDEX, -1 );

    if( index > -1 ){
      int pic = getImg( index );
      ImageView img = findViewById( R.id.ivImage );
      scaleImg( img, pic );
    }
  }

  private int getImg( int index ){
    return index % 2 == 0 ? R.drawable.carnevil : R.drawable.doge;
  }

  private void scaleImg( ImageView img, int pic ){
    Display screen = getWindowManager().getDefaultDisplay();
    BitmapFactory.Options options = new BitmapFactory.Options();

    options.inJustDecodeBounds = true;
    BitmapFactory.decodeResource( getResources(), pic, options );

    int imgWidth = options.outWidth;
    int screenWidth = screen.getWidth();
    if( imgWidth > screenWidth ){
      int ratio = Math.round((float)imgWidth / (float)screenWidth );
      options.inSampleSize = ratio;
    }
    options.inJustDecodeBounds = false;
    Bitmap scaledImg = BitmapFactory.decodeResource( getResources(), pic, options );
    img.setImageBitmap( scaledImg );
  }
}