package org.rhyv.testapp;

import android.os.Bundle;
import android.view.Menu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {
  public static final String EXTRA_TEXT       = "org.rhyv.testapp.TEXT";
  private AppBarConfiguration mAppBarConfiguration;

  @Override
  protected void onCreate( Bundle savedInstanceState ){
    super.onCreate( savedInstanceState );
    setContentView( R.layout.activity_main);
    AppCenter.start(getApplication(), "e696dc97-731a-4246-b0a1-6ccd6e238db7",
                  Analytics.class, Crashes.class);
    Toolbar toolbar = findViewById( R.id.toolbar );
    setSupportActionBar( toolbar );

    DrawerLayout drawer = findViewById( R.id.drawer_layout );
    NavigationView navigationView = findViewById( R.id.nav_view );
    // Passing each menu ID as a set of Ids because each menu should be considered as top level destinations.
    mAppBarConfiguration = new AppBarConfiguration.Builder( R.id.nav_home, R.id.nav_items ).setDrawerLayout( drawer ).build();
    NavController navController = Navigation.findNavController( this, R.id.nav_host_fragment );
    NavigationUI.setupActionBarWithNavController( this, navController, mAppBarConfiguration );
    NavigationUI.setupWithNavController( navigationView, navController );
  }

  @Override
  public boolean onCreateOptionsMenu( Menu menu ){
    getMenuInflater().inflate( R.menu.nav_drawer, menu );
    return true;
  }

  @Override
  public boolean onSupportNavigateUp(){
    NavController navController = Navigation.findNavController( this, R.id.nav_host_fragment );
    return NavigationUI.navigateUp( navController, mAppBarConfiguration ) || super.onSupportNavigateUp();
  }
}